<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('products')->insert([
            'name' => 'Banana',
            'price' => 3,
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        for($i=0; $i < 100; $i++){
            DB::table('products')->insert([
                'name' => rand(14,70),
                'price' => str_random(100,2000)/100,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
        }
     
    }
}
