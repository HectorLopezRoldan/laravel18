<?php

namespace Tests\Feature;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    /**
     * @group users
     */
    use RefreshDatabase;

    public function test_lista_de_usuarios_vacia()
    {
        $response = $this->get('/users');
        $response->assertStatus(200);
        $response->assertSee('Lista de usuarios');
        //$response->assertSee('No hay usuarios');
    }
    
    public function test_metodo_show_user_1()
  {
      factory(User::class)->create([
          'id' => 1,
          'name' => 'Pepe',
          'email' => 'pepe@gmail.com'
      ]);

      $response = $this->get('/users/1');
      $response->assertStatus(200);
      $response->assertSee('detalle del usuario 1');
      $response->assertSee('Pepe');
      $response->assertSee('pepe@gmail.com');
  }
  
  public function test_metodo_show_user_inexistente()
  {
      $response = $this->get('/users/100000');
      $response->assertStatus(404);
  }


    public function test_lista_de_usuarios()
    {
      $response=$this->get('/users');
      $response->assertStatus(200);
      $response->assertSee('Lista de usuarios');
      //$response->assertSee('pepe');
    }

    // public function test_show_usuario()
    // {
    //   $response=$this->get('/users/1');
    //   $response->assertStatus(200);
    //   $response->assertSee('Este es el detalle del usuario');
    // }
}
