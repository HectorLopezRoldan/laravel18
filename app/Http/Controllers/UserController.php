<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users=User::paginate();
       // dd($users); como var_dump


        return view('user.index',['users' => $users]);

        //busca el fichero (uno de los dos):
        // /resources/views/user/index.php
        // /resources/views/user/index.blade.php
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $user= new User();
        // $user->name=$request->input('name');
        // $user->age=$request->input('age');
        // $user->password=bcrypt($request->input('password'));
        // $user->email=$request->input('email');
        // $user->remember_token=str_random(10);
        // $user->save();
   
        // $user= new User($request->all());
        // $user->save();
        //oo
        // $user= User::create($request->all());
        // return redirect('users');
        //oo para usar esta forma habra que haber definido los campos en el modelo user
        //vease 
        // protected $fillable = [
        //     'age','name', 'email', 'password',
        // ];
        $user=new User();
        $user->fill($request->all());
        $user->save();
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        
        /*if($user==null){// pero el de arriba me redirige, aqui fuerzo yo ese error
            abort(404,'Prohibido');
        }*/

        return view('user.show', [
         'user' =>$user,
         ]);

        // $otro = "Otra variable....";
        // // dd(compact('id', 'otro'));
        // return view('user.show', compact('id', 'otro'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user =User::findOrFail($id);

        return view('user.edit',['user'=>$user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $user = User::findOrFail($id);
        $user->fill($request->all());
        $user->save();
        return redirect('/users/'.$user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
   
        $user = User::findOrFail($id);
        $user->delete();
        return back();
    }

    public function especial()
    {
        $users = User::Where('id','>=',12)->where('id','<=',23)->get();
        dd($users);

        //return redirect('/users');
    }

    
}
