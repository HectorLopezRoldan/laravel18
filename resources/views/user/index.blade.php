
@extends('layouts.app')
@section('title','Lista de usuarios')
@section('content')

    <h1>Es la lista de usuarios</h1>
    <a href="/users/create">Nuevo</a>
    <ul>
        @foreach ($users as $user)
         <li>{{$user->name}}</li>
         <li>{{$user->email}}</li>
         <li>{{$user->age}} <a href="/users/{{$user->id}}/edit"> editar</a> </li>

    <form action="/users/{{$user->id}}" method="post">
    {{csrf_field()}}
    <input type="hidden" name="_method" value="DELETE">
    <input type="submit" value="Borrar">
    </form>
    <hr>


         @endforeach
    </ul>

    {{$users->render()}}
@endsection
