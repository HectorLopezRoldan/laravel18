@extends('layouts.app')
@section('title','Lista de usuarios')
@section('content')
<h1>
        Editar usuario
    </h1>
    <form action="/users/{{$user->id}}" method="post">

 <input type="hidden" name="_method" value="PUT">
      {{csrf_field()}}

    <label for="name">Nombre</label>
    <input type="text" name="name" value="{{$user->name}}"><br>

    <label for="email">Email</label>

    <input type="text" name="email" value="{{$user->email}}"><br>
    <!-- <label for="password">Contraseña</label>
    <input type="text" name="password"> -->
    <label for="age">Edad</label>
    <input type="text" name="age" value="{{$user->age}}">
    <input type="submit" value="Editar">
    </form>
    @endsection
